<?php 
	require "../partials/template.php";

	function
		get_body_contents(){



 ?>

 		<div class="container">
 			<h1 class="text-center py-3">REGISTER</h1>
 			<div class="row">
 				<div class="col-lg-4 offset-lg-4">
 					<form action="../controllers/register-process.php" method="POST">
 						<div class="form-group">
 							<label for="firstName">First Name:</label>
 							<input type="text" name="firstName" class="form-control">
 							<label for="lastName">Last Name</label>
 							<input type="text" name="lastName" class="form-control">
 						</div>
 						<div class="form-group">
 							<label for="email">Email</label>
 							<input type="email" name="email" class="form-control">
 						</div>
 						<div class="form-group">
 							<label for="password">Password</label>
 							<input type="password" name="password" class="form-control">
 						</div>

 						<div class="text-center">
 							<button type="submit" class="btn btn-info">Register</button>
 						</div>


 					</form>
 				</div>
 			</div>
 		</div>

 <?php 

}

 ?>