<?php 
	require "../partials/template.php";
	function get_body_contents(){

 ?>
 	<div class="container">
 		<h1 class="text-container">Login</h1>
 		<div class="row">
 			<div class="col-lg-4 offset-lg-4">
 				<form action="../controllers/login-process.php" method="POST">
 					<div class="form-group">
 						<label for="email">Email</label>
 						<input type="email" name="email" class="form-control">
 					</div>
 					<div class="form-group">
 						<label for="password">Password</label>
 						<input type="password" name="password" class="form-control">
 					</div>
 					<div class="text-center">
 						<button type="submit" class="btn btn-info">Login</button>
 					</div>
 				</form>
 			</div>
 		</div>
 	</div>

 <?php 

 } 

 ?>