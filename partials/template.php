<!DOCTYPE html>
<html>
<head>
	<title></title>
<!-- Bootswatch -->
	<link rel="stylesheet" type="text/css" href="https://bootswatch.com/4/cyborg/bootstrap.css">
</head>
<body>
		<nav class="navbar navbar-expand-lg navbar-light bg-light">
		  <a class="navbar-brand" href="../index.php">Lapse Top</a>
		  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor03" aria-controls="navbarColor03" aria-expanded="false" aria-label="Toggle navigation">
		    <span class="navbar-toggler-icon"></span>
		  </button>

		  <div class="collapse navbar-collapse" id="navbarColor03">
		    <ul class="navbar-nav mr-auto">
		      <li class="nav-item active">
		        <a class="nav-link" href="../views/catalog.php">Bar Menu<span class="sr-only">(current)</span></a>
		      </li>
		      <li class="nav-item">
		        <a class="nav-link" href="add-item.php">Add Item</a>
		      </li>
		      <li class="nav-item">
		        <a class="nav-link" href="cart.php">Cart</a>
		      </li>
		      <li class="nav-item">
		        <a class="nav-link" href="#">About</a>
		      </li>
		    </ul>
		  </div>
		</nav>
		<!-- Page Contents -->
		<?php get_body_contents()





		?>
		<!-- Footer -->
		<footer class="page-footer font-small bg-light navbar-dark">
			<div class="footer-copyright text-center py-3">All rights reserved. 2020</div>
		</footer>
</body>
</html>