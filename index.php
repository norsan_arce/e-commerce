<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Isis #1</title>
	<!-- Bootswatch -->
	<link rel="stylesheet" type="text/css" href="https://bootswatch.com/4/cyborg/bootstrap.css">


</head>
<body>
	<!-- Navbar from Bootswatch -->
	<header>
		<nav class="navbar navbar-expand-lg navbar-light bg-light">
		  <a class="navbar-brand" href="#">Lapse Top</a>
		  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor03" aria-controls="navbarColor03" aria-expanded="false" aria-label="Toggle navigation">
		    <span class="navbar-toggler-icon"></span>
		  </button>

		  <div class="collapse navbar-collapse" id="navbarColor-3">
		    <ul class="navbar-nav mr-auto">
		      <li class="nav-item active">
		        <a class="nav-link" href="views/catalog.php">Bar Menu<span class="sr-only">(current)</span></a>
		      </li>
		      <li class="nav-item">
		        <a class="nav-link" href="views/add-item.php">Add Item</a>
		      </li>
		      <li class="nav-item">
		        <a class="nav-link" href="#">Cart</a>
		      </li>
		      <li class="nav-item">
		        <a class="nav-link" href="#">About</a>
		      </li>
		    </ul>
		  </div>
		</nav>
		  <div class="vh-100 d-flex justify-content-center align-items-center flex-column">
		  	<h1>Welcome to Lapse Top</h1>
		  	<a href="views/catalog.php" class="btn btn-success">View Menu</a>
		  </div>
		</nav>
	</header>

	<!-- Featured Products Page -->




	<section>
		<h1 class="text-center p-5">Featured Menu Items</h1>
		<div class="container">
			<div class="row">
				<?php
					$products = file_get_contents("assets/lib/products.json");
					// var_dump($products);
					$products_array = json_decode($products, true);
					// var_dump($products_array);
					for($i=0; $i<3; $i++){
						?>
						<div class="col-lg-4 py-2">
							<div class="card">
								<img src="assets/lib/<?php echo $products_array[$i]["image"] ?>" class="card-img-top" height="350px">
								<div class="card-body">
									<h5 class="card-title"><?php echo $products_array[$i]["name"]?></h5>
									<p class="card-text">Price: Php <?php echo $products_array[$i]["price"]?></p>
									<p class="card-text">Description: <?php echo $products_array[$i]["description"]?></p>
								</div>
							</div>
						</div>
				<?php
					}
				?>
			</div>
		</div>
	</section>
</body>
</html>